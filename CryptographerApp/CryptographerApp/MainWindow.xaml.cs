﻿using System;
using System.IO;
using System.Windows;

namespace CryptographerApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            loadButton.Click += LoadButton_Click;
            saveButton.Click += SaveButton_Click;
            calculateButton.Click += CalculateButton_Click;
        }

        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            SaveLoadWindow saveLoadWindow = new SaveLoadWindow(true);
            if (saveLoadWindow.ShowDialog() == true)
            {
                try
                {
                    string fullPathFile = TextDataFile.MakeFullPath(saveLoadWindow.PathFile, saveLoadWindow.NameFile + saveLoadWindow.TypeFile, saveLoadWindow.IsLoad);
                    inputTextBox.Text = TextDataFile.LoadFile(fullPathFile, saveLoadWindow.TypeFile);
                    MessageBox.Show($"Исходный текст загружен из файла {fullPathFile}", "Загрузка", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveLoadWindow saveLoadWindow = new SaveLoadWindow(false);
            if (saveLoadWindow.ShowDialog() == true)
            {
                try
                {
                    string fullPathFile = TextDataFile.MakeFullPath(saveLoadWindow.PathFile, saveLoadWindow.NameFile + saveLoadWindow.TypeFile, saveLoadWindow.IsLoad);
                    MessageBoxResult result = MessageBoxResult.Yes;
                    if (File.Exists(fullPathFile))
                    {
                        result = MessageBox.Show(
                            $"Файл {fullPathFile} уже существует.\nВы хотите заменить его?",
                            "Подтвердить сохранение",
                            MessageBoxButton.YesNo,
                            MessageBoxImage.Warning);
                    }
                    if (result == MessageBoxResult.Yes)
                    {
                        TextDataFile.SaveFile(fullPathFile, saveLoadWindow.TypeFile, outputTextBox.Text);
                        MessageBox.Show($"Преобразованный текст сохранен в файл {fullPathFile}", "Сохранение", MessageBoxButton.OK, MessageBoxImage.Information);
                    }                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void CalculateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (isEncrypt.IsChecked == true)
                {
                    outputTextBox.Text = VigenereCode.Encrypt(inputTextBox.Text, keyTextBox.Text);
                    SaveButton_Click(null, null);
                }
                else
                {
                    outputTextBox.Text = VigenereCode.Decrypt(inputTextBox.Text, keyTextBox.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
