﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.IO;

namespace CryptographerApp
{
    /// <summary>
    /// Логика взаимодействия для SaveLoadWindow.xaml
    /// </summary>
    public partial class SaveLoadWindow : Window
    {
        public SaveLoadWindow(bool isLoad)
        {
            InitializeComponent();
            pathFileTextBox.Text = Path.GetFullPath(@"..\..\..\User_data\");
            nameFileTextBox.Text = "MySaveFile";
            if (isLoad)
            {
                Title = "Загрузка";
                acceptButton.Content = "Загрузить";
            }
            IsLoad = isLoad;
        }
        
        public string PathFile
        {
            get { return pathFileTextBox.Text; }
        }
        public string NameFile
        {
            get { return nameFileTextBox.Text; }
        }
        public string TypeFile
        {
            get
            {
                TextBlock typeFileTextBlock = (TextBlock)((ComboBoxItem)typeFileComboBox.SelectedItem).Content;
                string typeFullText = Convert.ToString(typeFileTextBlock.Text);
                string typeTempText = typeFullText.Substring(typeFullText.IndexOf('.'));
                return typeTempText.Substring(0, typeTempText.Length - 1);
            }
        }
        public bool IsLoad { get; set; }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

    }
}

