﻿using Microsoft.Office.Interop.Word;
using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace CryptographerApp
{
    public static class TextDataFile
    {
        public static void SaveFile(string fullPathFile, string typeFile, string message)
        {
                switch (typeFile)
                {
                    case ".txt":
                        TextDataFile.SaveFileTxt(fullPathFile, message);
                        break;
                    case ".docx":
                        TextDataFile.SaveFileDocx(fullPathFile, message);
                        break;
                }            
        }

        public static string LoadFile(string fullPathFile, string typeFile)
        {
            string result = "";
            switch (typeFile)
            {
                case ".txt":
                    result = LoadFileTxt(fullPathFile);
                    break;
                case ".docx":
                    result = LoadFileDocx(fullPathFile);
                    break;
            }
            return result;
        }

        public static string MakeFullPath(string pathFile, string nameFile, bool isLoad)
        {
            if (!Directory.Exists(pathFile))
            {
                throw new DirectoryNotFoundException("Указан неверный путь к файлу!");
            }

            if (isLoad)
            {
                if (!File.Exists(pathFile + nameFile))
                {
                    throw new FileNotFoundException("Файла с указанным именем не существует!");
                }
            }
            else
            {
                Regex regex = new Regex(@"[\\\/\|\:\*\?\<\>\\""]");
                MatchCollection matches = regex.Matches(nameFile);
                if (matches.Count > 0)
                {
                    throw new ArgumentException("Имя файла не должно содержать следующих знаков:\n\t \\ / : * ? \" < > |");
                }
            }

            return pathFile + nameFile;
        }

        private static void SaveFileTxt(string fullPath, string message)
        {
            using (File.Create(fullPath)) { }
            File.WriteAllText(fullPath, message, Encoding.GetEncoding(1251));
        }

        private static void SaveFileDocx(string fullPath, string message)
        {
            Application app = new Application();
            Document doc = app.Documents.Add();
            app.Selection.TypeText(message);
            app.ActiveDocument.SaveAs2(fullPath);
            doc.Close();
            app.Quit();
            doc = null;
            app = null;
            GC.Collect();
        }     

        private static string LoadFileTxt(string fullPath)
        {           
            return File.ReadAllText(fullPath, Encoding.GetEncoding(1251));            
        }

        private static string LoadFileDocx(string fullPath)
        {
            string message;
            Application app = new Application();
            Document doc = app.Documents.Open(fullPath);
            message = doc.Content.Text;
            doc.Close();
            app.Quit();
            doc = null;
            app = null;
            GC.Collect();
            return message;
        }       
    }
}
