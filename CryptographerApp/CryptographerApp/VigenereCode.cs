﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptographerApp
{
    public static class VigenereCode
    {
        private static string alp = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
        public static string Alphabet {
             get { return alp;  }
             set { alp = value.ToUpper(); }
        }

        public static string Encrypt(string message, string key) => Calculate(message, key, false);
        public static string Decrypt(string message, string key) => Calculate(message, key, true);

        private static string Calculate(string message, string key, bool mode)
        {
            string alpKey = "";
            foreach (var letter in key)
            {
                if (alp.Contains(char.ToUpper(letter)))
                {
                    alpKey += letter;
                }
            }
            if (string.IsNullOrEmpty(alpKey))
            {
                throw new ArgumentException(string.IsNullOrEmpty(key) ? "Необходимо указать ключ!" : "Неверный алфавит ключа!");
            }

            key = alpKey.ToUpper();
            string res = "";
            int keyIndex = 0;
            for (int i = 0; i < message.Length; i++)
            {
                if (alp.Contains(char.ToUpper(message[i])))
                {
                    int mesLetterIndex = alp.IndexOf(char.ToUpper(message[i]));
                    char keyLetter = key[keyIndex % key.Length];
                    int keyLetterIndex = alp.IndexOf(keyLetter);

                    keyLetterIndex = mode ? -keyLetterIndex : keyLetterIndex;
                    char resLetter = alp[(alp.Length + mesLetterIndex + keyLetterIndex) % alp.Length];
                    res += char.IsUpper(message[i]) ? resLetter : char.ToLower(resLetter);
                    keyIndex++;
                }
                else
                {
                    res += message[i];
                }
            }
            return res;
        }
    }
}
