﻿using CryptographerApp;
using Microsoft.Office.Interop.Word;
using System;
using System.IO;
using System.Text;
using Xunit;


namespace CryptographerAppTests
{
    public class TextDataFileTests
    {
        public TextDataFileTests()
        {
            if (!Directory.Exists(@"..\..\..\..\Test_data"))
            {
                Directory.CreateDirectory(@"..\..\..\..\Test_data");
            }
        }

        [Fact]
        public void LoadFileTxtRightСontent()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "RightСontentGettingByLoadFileTxt.txt";
            string fullPathFile = pathFile + nameFile;
            using (File.Create(fullPathFile)) { }
            File.WriteAllText(fullPathFile, "Карл у Клары украл кораллы", Encoding.GetEncoding(1251));
            string result = TextDataFile.LoadFile(fullPathFile, ".txt");
            File.Delete(fullPathFile);
            Assert.Equal("Карл у Клары украл кораллы", result);
        }
        [Fact]
        public void SaveFileTxtExist()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "FileTxtExistAfterSave.txt";
            string fullPathFile = pathFile + nameFile;
            TextDataFile.SaveFile(fullPathFile, ".txt", "Карл у Клары украл кораллы");
            bool result = File.Exists(fullPathFile);
            File.Delete(fullPathFile);
            Assert.True(result);            
        }
        [Fact]
        public void SaveFileTxtRightСontent()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "FileTxtRightСontentAfterSave.txt";
            string fullPathFile = pathFile + nameFile;
            TextDataFile.SaveFile(fullPathFile, ".txt", "Карл у Клары украл кораллы");
            string result = File.ReadAllText(fullPathFile, Encoding.GetEncoding(1251));
            File.Delete(fullPathFile);
            Assert.Equal("Карл у Клары украл кораллы", result);            
        }
        [Fact]
        public void SaveFileTxtExistWhenExistFileWithSameName()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "FileTxtExistAfterSaveWhenExistFileWithSameName.txt";
            string fullPathFile = pathFile + nameFile;
            using (File.Create(fullPathFile)) { }
            TextDataFile.SaveFile(fullPathFile, ".txt", "Карл у Клары украл кораллы");
            bool result = File.Exists(fullPathFile);
            File.Delete(fullPathFile);
            Assert.True(result);            
        }
        [Fact]
        public void SaveFileTxtRightСontentWhenExistFileWithSameName()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "FileTxtRightСontentAfterSaveWhenExistFileWithSameName.txt";
            string fullPathFile = pathFile + nameFile;
            using (File.Create(fullPathFile)) { }
            TextDataFile.SaveFile(fullPathFile, ".txt", "Карл у Клары украл кораллы");
            string result = File.ReadAllText(fullPathFile, Encoding.GetEncoding(1251));
            File.Delete(fullPathFile);
            Assert.Equal("Карл у Клары украл кораллы", result);
        }

        [Fact]
        public void LoadFileDocxRightСontent()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "RightСontentGettingByLoadFileDocx.docx";
            string fullPathFile = pathFile + nameFile;
            Application app = new Application();
            Document doc = app.Documents.Add();
            app.Selection.TypeText("Карл у Клары украл кораллы");
            app.ActiveDocument.SaveAs2(fullPathFile);
            doc.Close();
            app.Quit();
            doc = null;
            app = null;
            GC.Collect();
            string result = TextDataFile.LoadFile(fullPathFile, ".docx");
            File.Delete(fullPathFile);
            Assert.Equal("Карл у Клары украл кораллы\r", result);
        }
        [Fact]
        public void SaveFileDocxExist()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "FileDocxExistAfterSave.docx";
            string fullPathFile = pathFile + nameFile;
            TextDataFile.SaveFile(fullPathFile, ".docx", "Карл у Клары украл кораллы");
            bool result = File.Exists(fullPathFile);
            File.Delete(fullPathFile);
            Assert.True(result);
        }
        [Fact]
        public void SaveFileDocxRightСontent()
        {
            string pathFile =  Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "FileDocxRightСontentAfterSave.docx";
            string fullPathFile = pathFile + nameFile;
            TextDataFile.SaveFile(fullPathFile, ".docx", "Карл у Клары украл кораллы");
            Application app = new Application();
            Document doc = app.Documents.Open(fullPathFile);
            string result = doc.Content.Text;
            doc.Close();
            app.Quit();
            doc = null;
            app = null;
            GC.Collect();
            File.Delete(fullPathFile);
            Assert.Equal("Карл у Клары украл кораллы\r", result);
        }
        [Fact]
        public void SaveFileDocxExistWhenExistFileWithSameName()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "FileDocxExistAfterSaveWhenExistFileWithSameName.docx";
            string fullPathFile = pathFile + nameFile;
            using (File.Create(fullPathFile)) { }
            TextDataFile.SaveFile(fullPathFile, ".docx", "Карл у Клары украл кораллы");
            bool result = File.Exists(fullPathFile);
            File.Delete(fullPathFile);
            Assert.True(result);
        }
        [Fact]
        public void SaveFileDocxRightСontentWhenExistFileWithSameName()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "FileDocxRightСontentAfterSaveWhenExistFileWithSameName.docx";
            string fullPathFile = pathFile + nameFile;
            using (File.Create(fullPathFile)) { }
            TextDataFile.SaveFile(fullPathFile, ".docx", "Карл у Клары украл кораллы");
            Application app = new Application();
            Document doc = app.Documents.Open(fullPathFile);
            string result = doc.Content.Text;
            doc.Close();
            app.Quit();
            doc = null;
            app = null;
            GC.Collect();
            File.Delete(fullPathFile);
            Assert.Equal("Карл у Клары украл кораллы\r", result);
        }

        [Fact]
        public void MakeFullPathWithRightPathAndNameWhenDoLoad()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "MakeFullPathWithRightPathAndNameWhenDoLoad.txt";
            string fullPathFile = pathFile + nameFile;
            using (File.Create(fullPathFile)) { }
            string result = TextDataFile.MakeFullPath(pathFile, nameFile, true);
            File.Delete(fullPathFile);
            Assert.Equal(fullPathFile, result);
        }
        [Fact]
        public void MakeFullPathWithWrongPathWhenDoLoad()
        {
            string pathFile = @"C:\Users\moks\CourseNowYouSeeSharp\course-work\CryptographerApp\Test123\";
            string nameFile = "MakeFullPathWithWrongPathWhenDoLoad.txt";
            Exception ex = Record.Exception(() => TextDataFile.MakeFullPath(pathFile, nameFile, true));
            Assert.IsType<DirectoryNotFoundException>(ex);
            Assert.Equal("Указан неверный путь к файлу!", ex.Message);
        }
        [Fact]
        public void MakeFullPathWithWrongNameWhenDoLoad()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "Test123.txt";
            Exception ex = Record.Exception(() => TextDataFile.MakeFullPath(pathFile, nameFile, true));
            Assert.IsType<FileNotFoundException>(ex);
            Assert.Equal("Файла с указанным именем не существует!", ex.Message);
        }
        [Fact]
        public void MakeFullPathWithRightPathAndNameWhenDoSave()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "MakeFullPathWithRightPathAndNameWhenDoSave.txt";
            string fullPathFile = pathFile + nameFile;
            string result = TextDataFile.MakeFullPath(pathFile, nameFile, false);
            Assert.Equal(fullPathFile, result);
        }
        [Fact]
        public void MakeFullPathWithWrongPathWhenDoSave()
        {
            string pathFile = @"C:\Users\moks\CourseNowYouSeeSharp\course-work\CryptographerApp\Test123\";
            string nameFile = "MakeFullPathWithWrongPathWhenDoSave.txt";
            Exception ex = Record.Exception(() => TextDataFile.MakeFullPath(pathFile, nameFile, false));
            Assert.IsType<DirectoryNotFoundException>(ex);
            Assert.Equal("Указан неверный путь к файлу!", ex.Message);
        }
        [Fact]
        public void MakeFullPathWithWrongNameWhenDoSave1()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "Test\\.txt";
            Exception ex = Record.Exception(() => TextDataFile.MakeFullPath(pathFile, nameFile, false));
            Assert.IsType<ArgumentException>(ex);
            Assert.Equal("Имя файла не должно содержать следующих знаков:\n\t \\ / : * ? \" < > |", ex.Message);
        }
        [Fact]
        public void MakeFullPathWithWrongNameWhenDoSave2()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "Test/.txt";
            Exception ex = Record.Exception(() => TextDataFile.MakeFullPath(pathFile, nameFile, false));
            Assert.IsType<ArgumentException>(ex);
            Assert.Equal("Имя файла не должно содержать следующих знаков:\n\t \\ / : * ? \" < > |", ex.Message);
        }
        [Fact]
        public void MakeFullPathWithWrongNameWhenDoSave3()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "Test:.txt";
            Exception ex = Record.Exception(() => TextDataFile.MakeFullPath(pathFile, nameFile, false));
            Assert.IsType<ArgumentException>(ex);
            Assert.Equal("Имя файла не должно содержать следующих знаков:\n\t \\ / : * ? \" < > |", ex.Message);
        }
        [Fact]
        public void MakeFullPathWithWrongNameWhenDoSave4()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "Test*.txt";
            Exception ex = Record.Exception(() => TextDataFile.MakeFullPath(pathFile, nameFile, false));
            Assert.IsType<ArgumentException>(ex);
            Assert.Equal("Имя файла не должно содержать следующих знаков:\n\t \\ / : * ? \" < > |", ex.Message);
        }
        [Fact]
        public void MakeFullPathWithWrongNameWhenDoSave5()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "Test?.txt";
            Exception ex = Record.Exception(() => TextDataFile.MakeFullPath(pathFile, nameFile, false));
            Assert.IsType<ArgumentException>(ex);
            Assert.Equal("Имя файла не должно содержать следующих знаков:\n\t \\ / : * ? \" < > |", ex.Message);
        }
        [Fact]
        public void MakeFullPathWithWrongNameWhenDoSave6()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "Test\".txt";
            Exception ex = Record.Exception(() => TextDataFile.MakeFullPath(pathFile, nameFile, false));
            Assert.IsType<ArgumentException>(ex);
            Assert.Equal("Имя файла не должно содержать следующих знаков:\n\t \\ / : * ? \" < > |", ex.Message);
        }
        [Fact]
        public void MakeFullPathWithWrongNameWhenDoSave7()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "Test<.txt";
            Exception ex = Record.Exception(() => TextDataFile.MakeFullPath(pathFile, nameFile, false));
            Assert.IsType<ArgumentException>(ex);
            Assert.Equal("Имя файла не должно содержать следующих знаков:\n\t \\ / : * ? \" < > |", ex.Message);
        }
        [Fact]
        public void MakeFullPathWithWrongNameWhenDoSave8()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "Test>.txt";
            Exception ex = Record.Exception(() => TextDataFile.MakeFullPath(pathFile, nameFile, false));
            Assert.IsType<ArgumentException>(ex);
            Assert.Equal("Имя файла не должно содержать следующих знаков:\n\t \\ / : * ? \" < > |", ex.Message);
        }
        [Fact]
        public void MakeFullPathWithWrongNameWhenDoSave9()
        {
            string pathFile = Path.GetFullPath(@"..\..\..\..\Test_data\");
            string nameFile = "Test|.txt";
            Exception ex = Record.Exception(() => TextDataFile.MakeFullPath(pathFile, nameFile, false));
            Assert.IsType<ArgumentException>(ex);
            Assert.Equal("Имя файла не должно содержать следующих знаков:\n\t \\ / : * ? \" < > |", ex.Message);
        }
    }
}
