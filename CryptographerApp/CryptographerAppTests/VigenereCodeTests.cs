using CryptographerApp;
using System;
using Xunit;

namespace CryptographerAppTests
{
    public class VigenereCodeTests
    {        
        [Fact]
        public void EncryptWithStandartKey()
        {
            string result = VigenereCode.Encrypt("���� � ����� ����� �������", "��������");
            Assert.Equal("���� � ���� ����� �������", result);
        }
        [Fact]
        public void EncryptWithChangedKey()
        {
            string result = VigenereCode.Encrypt("���� � ����� ����� �������", "�������");
            Assert.Equal("���� � ����� ����� �������", result);
        }
        [Fact]
        public void EncryptEmptyMessage()
        {
            string result = VigenereCode.Encrypt("", "�������");
            Assert.Equal("", result);
        }
        [Fact]
        public void EncryptMessageContainingOnlyIrregularChars()
        {
            string result = VigenereCode.Encrypt("Karl stole corals from Clara", "�������");
            Assert.Equal("Karl stole corals from Clara", result);
        }
        [Fact]
        public void EncryptWithKeyContainingIrregularChars1()
        {
            string result = VigenereCode.Encrypt("���� � ����� ����� �������", "�������123");
            Assert.Equal("���� � ����� ����� �������", result);
        }
        [Fact]
        public void EncryptWithKeyContainingIrregularChars2()
        {
            string result = VigenereCode.Encrypt("���� � ����� ����� �������", "123���123����123");
            Assert.Equal("���� � ����� ����� �������", result);
        }
        [Fact]
        public void EncryptWithEmptyKey()
        {
            Exception ex = Record.Exception(() => VigenereCode.Encrypt("���� � ����� ����� �������", ""));
            Assert.IsType<ArgumentException>(ex);
            Assert.Equal("���������� ������� ����!", ex.Message);
        }
        [Fact]
        public void EncryptWithKeyContainingOnlyIrregularChars()
        {
            Exception ex = Record.Exception(() => VigenereCode.Encrypt("���� � ����� ����� �������", "qwerty123"));
            Assert.IsType<ArgumentException>(ex);
            Assert.Equal("�������� ������� �����!", ex.Message);
        }

        [Fact]
        public void DecryptWithStandartKey()
        {
            string result = VigenereCode.Decrypt("���� � ���� ����� �������", "��������");
            Assert.Equal("���� � ����� ����� �������", result); 
        }
        [Fact]
        public void DecryptWithChangedKey()
        {
            string result = VigenereCode.Decrypt("���� � ����� ����� �������", "�������");
            Assert.Equal("���� � ����� ����� �������", result); 
        }
        [Fact]
        public void DecryptEmptyMessage()
        {
            string result = VigenereCode.Decrypt("", "�������");
            Assert.Equal("", result);
        }
        [Fact]
        public void DecryptMessageContainingOnlyIrregularChars()
        {
            string result = VigenereCode.Decrypt("Karl stole corals from Clara", "�������");
            Assert.Equal("Karl stole corals from Clara", result);
        }
        [Fact]
        public void DecryptWithKeyContainingIrregularChars1()
        {
            string result = VigenereCode.Decrypt("���� � ����� ����� �������", "�������123");
            Assert.Equal("���� � ����� ����� �������", result); 
        }
        [Fact]
        public void DecryptWithKeyContainingIrregularChars2()
        {
            string result = VigenereCode.Decrypt("���� � ����� ����� �������", "QWE���123����123");
            Assert.Equal("���� � ����� ����� �������", result);
        }
        [Fact]
        public void DecryptWithEmptyKey()
        {
            Exception ex = Record.Exception(() => VigenereCode.Decrypt("���� � ����� ����� �������", ""));
            Assert.IsType<ArgumentException>(ex);
            Assert.Equal("���������� ������� ����!", ex.Message);
        }
        [Fact]
        public void DecryptWithKeyContainingOnlyIrregularChars()
        {
            Exception ex = Record.Exception(() => VigenereCode.Decrypt("���� � ����� ����� �������", "qwerty123"));
            Assert.IsType<ArgumentException>(ex);
            Assert.Equal("�������� ������� �����!", ex.Message);
        }

    }
}
